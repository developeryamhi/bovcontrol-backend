Repo => https://bitbucket.org/developeryamhi/bovcontrol-backend


#CAVITY-MAP => https://www.hackerrank.com/challenges/cavity-map/submissions/code/63124740
<?php

$handle = fopen ("php://stdin", "r");
function cavityMap($grid) {
    $len = sizeof( $grid );
    for( $i = 1; $i < $len - 1; $i++ )
    {
        for( $j = 1; $j < $len - 1; $j++ )
        {
            if( $grid[$i][$j] > $grid[$i][$j - 1] && $grid[$i][$j] > $grid[$i][$j + 1]
              && $grid[$i][$j] > $grid[$i-1][$j] && $grid[$i][$j] > $grid[$i+1][$j] )  $grid[$i][$j] = 'X';
        }
    }
    return $grid;
}

fscanf($handle, "%i",$n);
$grid = array();
for($grid_i = 0; $grid_i < $n; $grid_i++){
   fscanf($handle,"%s",$grid[]);
}
$result = cavityMap($grid);
echo implode("\n", $result)."\n";

?>


===========================================================================================================


#LILY'S-HOMEWORK => https://www.hackerrank.com/challenges/lilys-homework/submissions/code/63126598
#!/bin/ruby

def lilysHomework(arr)
    map = arr.each_with_index.map{|x,i| [x,i]}.to_h
    swaps = 0
    sort = arr.sort
    for i in 0...arr.size do
        if sort[i] != arr[i]
            swaps += 1
            map[arr[i]] = map[sort[i]]
            arr[map[sort[i]]], arr[i] = arr[i], sort[i]
        end
    end
    swaps
end

n = gets.strip.to_i
arr = gets.strip
arr = arr.split(' ').map(&:to_i)
rev_arr = arr.reduce([]) {|acc, x| [x] + acc}
result = lilysHomework(arr)
result2 = lilysHomework(rev_arr)
puts [result, result2].min


===========================================================================================================


#NON-DIVISIBLE-SUBSET => https://www.hackerrank.com/challenges/non-divisible-subset/submissions/code/63129894
#!/bin/python

import sys

def nonDivisibleSubset(k, numbers):
    counts = [0] * k
    for number in numbers:
        counts[number % k] += 1

    count = min(counts[0], 1)
    for i in range(1, k//2+1):
        if i != k - i:
            count += max(counts[i], counts[k-i])
    if k % 2 == 0: 
        count += 1

    return count

if __name__ == "__main__":
    n, k = raw_input().strip().split(' ')
    n, k = [int(n), int(k)]
    arr = map(int, raw_input().strip().split(' '))
    result = nonDivisibleSubset(k, arr)
    print result


===========================================================================================================


#NEW-YEAR-CHAOS => https://www.hackerrank.com/challenges/new-year-chaos/submissions/code/63130855
#!/bin/python3

import sys

def minimumBribes(n, q):
    res,pos = n,{1,2}
    for i in range(n):
        pos |= {i+3}
        if q[i] in pos:
            if q[i] == min(pos): res -= 1
            if q[i] == max(pos): res += 1
            pos -= {q[i]}
        else:
            print('Too chaotic')
            break
    else:
        print(res)

if __name__ == "__main__":
    t = int(input().strip())
    for a0 in range(t):
        n = int(input().strip())
        q = list(map(int, input().strip().split(' ')))
        minimumBribes(n, q)


===========================================================================================================


#BOMBER-MAN => https://www.hackerrank.com/challenges/bomber-man/submissions/code/63132289
#!/bin/python

import sys

# Enter your code here. Read input from STDIN. Print output to STDOUT
def bomb(r, c, grid, t):
    complete_grid = [ ["O"]*c for i in range(r) ]
    stable_grid = [ ["O"]*c for i in range(r) ]
    other_grid = [ ["O"]*c for i in range(r) ]
    for i in range(r):
        for j in range(c):
            if grid[i][j] == "O":
                other_grid[i][j] = "."
                if i>0:
                    other_grid[i-1][j] = "."
                if j>0:
                    other_grid[i][j-1] = "."
                if i<r-1:
                    other_grid[i+1][j] = "."
                if j<c-1:
                    other_grid[i][j+1] = "."
    for i in range(r):
        for j in range(c):
            if other_grid[i][j] == "O":
                stable_grid[i][j] = "."
                if i>0:
                    stable_grid[i-1][j] = "."
                if j>0:
                    stable_grid[i][j-1] = "."
                if i<r-1:
                    stable_grid[i+1][j] = "."
                if j<c-1:
                    stable_grid[i][j+1] = "."
    #print grid
    #print complete_grid
    #print other_grid
    if t==0 or t==1:
        return grid
    if (t-1)%2==0 and not (t-1)%4==0:
        return other_grid
    if (t-1)%4==0:
        return stable_grid
    if t%2==0:
        return complete_grid
grid = []
r, c, t = map(int, raw_input().split())
for _ in range(r):
    row = [ l for l in raw_input() ]
    grid.append(row)
res = "\n".join([ "".join(line) for line in bomb(r, c, grid, t) ]) 
print res


===========================================================================================================


#15-DAYS-OF-LEARNING_SQL => https://www.hackerrank.com/challenges/15-days-of-learning-sql/submissions/database/63132934
SELECT s1.submission_date,
     ( SELECT COUNT(distinct hacker_id)
        FROM Submissions s2
        WHERE s2.submission_date = s1.submission_date AND
            (SELECT COUNT(distinct s3.submission_date)
                FROM Submissions s3
                WHERE s3.hacker_id = s2.hacker_id
                    AND s3.submission_date < s1.submission_date) = DATEDIFF(s1.submission_date , '2016-03-01') ),
                    
      (SELECT hacker_id
        FROM Submissions s2
        WHERE s2.submission_date = s1.submission_date 
        GROUP BY hacker_id order by count(submission_id) DESC , hacker_id LIMIT 1) as max_submissions,

      (SELECT name FROM hackers WHERE hacker_id = max_submissions)

    FROM (SELECT DISTINCT submission_date from Submissions) s1
    GROUP BY s1.submission_date
    ORDER BY s1.submission_date ASC;


===========================================================================================================


#OCCUPATIONS => https://www.hackerrank.com/challenges/occupations/submissions/code/63134912
SET @r1=0, @r2=0, @r3=0, @r4=0;
SELECT MIN(Doctor), MIN(Professor), MIN(Singer), MIN(Actor)
FROM(
  select case when Occupation='Doctor' then (@r1:=@r1+1)
            when Occupation='Professor' then (@r2:=@r2+1)
            when Occupation='Singer' then (@r3:=@r3+1)
            when Occupation='Actor' then (@r4:=@r4+1) end as RowNumber,
    case when Occupation='Doctor' then Name end as Doctor,
    case when Occupation='Professor' then Name end as Professor,
    case when Occupation='Singer' then Name end as Singer,
    case when Occupation='Actor' then Name end as Actor
  from OCCUPATIONS
  order by Name
) Temp
group by RowNumber;


===========================================================================================================


#THE-PADS => https://www.hackerrank.com/challenges/the-pads/submissions/code/63135112
SELECT concat(NAME,concat("(",concat(substr(OCCUPATION,1,1),")"))) FROM OCCUPATIONS ORDER BY NAME ASC;

SELECT "There are a total of ", count(OCCUPATION), concat(lower(occupation),"s.") FROM OCCUPATIONS GROUP BY OCCUPATION ORDER BY count(OCCUPATION), OCCUPATION ASC


===========================================================================================================


#WEATHER-OBSERVATION-STATION => https://www.hackerrank.com/challenges/weather-observation-station-20/submissions/code/63135406
Select ROUND(S.LAT_N,4) AS mediam
    FROM STATION S
    WHERE
        (SELECT COUNT(Lat_N) FROM STATION WHERE Lat_N < S.LAT_N ) = (SELECT COUNT(Lat_N) from STATION WHERE Lat_N > S.LAT_N);
